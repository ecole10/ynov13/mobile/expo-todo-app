import * as React from 'react';
import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';
import {Card} from 'react-native-paper';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {
    faCheckCircle,
    faTimesCircle,
    faTrashAlt
} from '@fortawesome/free-solid-svg-icons';
import Button from "./Button";

const Todo = (props) => {
    return (
        <View style={styles.cardWrapper} key={`${props.item.id}`}>
            <TouchableOpacity
                onPress={() => props.toggleTodo(props.item.id)}
                style={styles.card}
            >
                <Card
                    style={[
                        {padding: 16},
                        {backgroundColor: props.color},
                        props.item.completed
                            ? styles.cardCompleted
                            : undefined,
                    ]}
                >
                    <FontAwesomeIcon
                        icon={props.item.completed ? faCheckCircle : faTimesCircle}
                        color={props.item.completed ? '#2E7D32' : '#C62828'}
                    />
                    <Text
                        style={[
                            styles.cardTitle,
                            props.item.completed
                                ? styles.cardTitleCompleted
                                : undefined,
                        ]}
                    >
                        {props.item.title}
                    </Text>
                </Card>

            </TouchableOpacity>
            <Button deactivate={props.item.completed === false} onPress={() => props.removeTodo(props.item.id)} icon={faTrashAlt} color="#C62828" />
        </View>
    );
}

const styles = StyleSheet.create({
    cardWrapper: {
        display: "flex",
        flexDirection: "row",
        marginBottom: 16,
    },
    card: {
        flex: 1,
        marginRight: 8,
    },
    cardCompleted: {
        opacity: 0.4,
    },
    cardTitle: {
        fontSize: 16,
        textAlign: "center",
        color: '#000',
    },
    cardTitleCompleted: {
        textDecorationLine: 'line-through',
    },
});

export default Todo;
