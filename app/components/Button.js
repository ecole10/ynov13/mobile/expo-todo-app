import React from 'react';
import {TouchableHighlight, Text, StyleSheet} from "react-native";
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';

const buttonStyles = StyleSheet.create({
    container: {
        borderRadius: 8,
        padding: 8,
        display: "flex",
        justifyContent: "center",
    },
    disabled: {
        backgroundColor: '#BDBDBD',
    }
})

const Button = (props) => {
    const color = props.color ? props.color : "#2962FF";
    const icon = props.icon ? <FontAwesomeIcon icon={props.icon}/> : <Text style={{textAlign: 'center'}}>{props.texte}</Text>;

    return (
        <TouchableHighlight
            disabled={props.deactivate}
            onPress={props.onPress}
            underlayColor="#000088"
            style={[
                buttonStyles.container,
                {backgroundColor: color},
                props.deactivate
                    ? buttonStyles.disabled
                    : undefined,
            ]}>
            {icon}
        </TouchableHighlight>
    )
};

export default Button;
