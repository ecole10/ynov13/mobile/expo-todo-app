import React, {useEffect, useState} from 'react';

import TodoList from './screens/TodoList';
import AddTodo from './screens/AddTodo';

export default function App() {
    const [isAdding, setIsAdding] = useState(false);
    const [loading, setLoading] = useState(true);
    const [todos, setTodos] = useState([]);

    useEffect(() => {
        if (todos.length === 0) {
            fetch('https://jsonplaceholder.typicode.com/todos?_start=0&_limit=10')
                .then((response) => response.json())
                .then((data) => {
                    setTodos(data);
                    setLoading(false);
                })
                .catch((err) => console.log(err));
        }
    }, []);

    const addTodo = (newTitle) => {
        const idMax = todos.length > 0 ? todos[todos.length - 1].id : 0;
        const newTodos = [
            ...todos,
            {
                id: idMax + 1,
                title: newTitle,
                completed: false,
            },
        ];
        setTodos(newTodos);
    };

    const toggleTodo = (todoId) => {
        const newTodos = todos.map((todo) => {
            if (todo.id === todoId) {
                return {
                    ...todo,
                    completed: !todo.completed,
                };
            }
            return todo;
        });
        setTodos(newTodos);
    };

    const removeTodo = (todoId) => {
        const newTodos = todos.filter((todo) => todo.id !== todoId);
        setTodos(newTodos);
    };

    return isAdding
        ? <AddTodo onBack={() => setIsAdding(false)} onAdd={addTodo}/>
        : <TodoList onAdd={() => setIsAdding(true)}
                    loading={loading}
                    items={todos}
                    toggleTodo={toggleTodo}
                    removeTodo={removeTodo}
          />;
}
