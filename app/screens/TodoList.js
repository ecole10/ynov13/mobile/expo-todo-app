import React from 'react';
import {
    Text,
    View,
    ActivityIndicator,
    ScrollView,
} from 'react-native';
import screensStyles from './ScreensStyles';
import {faPlus} from '@fortawesome/free-solid-svg-icons';

import Todo from '../components/Todo';
import Button from "../components/Button";

const TodoList = (props) => (
    <View style={screensStyles.container}>
        <View style={screensStyles.header}>
            <Text style={screensStyles.paragraph}>Todo</Text>
            <Button onPress={props.onAdd} icon={faPlus} />
        </View>

        <ScrollView>
            {
                props.loading
                    ? <ActivityIndicator size="large" color="#FFF"/>
                    : props.items.map((todoItem) =>
                        <Todo item={todoItem}
                              color="#FFF"
                              key={`${todoItem.id}`}
                              toggleTodo={props.toggleTodo}
                              removeTodo={props.removeTodo}
                        />
                      )
            }
        </ScrollView>
    </View>
);

export default TodoList;