import React, {useState} from 'react';
import {
    Text,
    View,
    StyleSheet,
    TextInput,
} from 'react-native';
import screensStyles from './ScreensStyles';
import {faArrowLeft, faPlus} from '@fortawesome/free-solid-svg-icons';
import Button from "../components/Button";

const addTodoStyles = StyleSheet.create({
    input: {
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderRadius: 8,
        padding: 16,
        marginBottom: 16,
    }
})

const AddTodo = (props) => {
    const [title, setTitle] = useState("");

    return (
        <View style={[screensStyles.container, { justifyContent: "flex-start" }]}>
            <View style={{ width: 30, marginBottom: 32, marginTop: 24 }}>
                <Button onPress={props.onBack} icon={faArrowLeft} />
            </View>

            <Text style={screensStyles.paragraph}>Nouvelle Todo</Text>
            <TextInput style={[addTodoStyles.input, {marginBottom: 16}]} onChangeText={(newText) => setTitle(newText)} value={title} />
            <Button onPress={() => {
                        props.onAdd(title);
                        props.onBack();
                    }}
                    texte="Ajouter"
            >
            </Button>
        </View>
    )
}

export default AddTodo;